import * as React from 'react';
import { ThemeProvider } from 'styled-components';
import theme from './theme';
import Main from './pages/Main';

const App = () => {
  return (
    <ThemeProvider theme={theme}>
      <Main />
    </ThemeProvider>
  );
};

export default App;
