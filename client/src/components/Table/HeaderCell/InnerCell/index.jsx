import React from 'react';
import Styled from 'styled-components';
import PropTypes from 'prop-types';

const InnerCell = ({className, children}) => {
  return (
    <div className={className}>
      {children}
    </div>
  );
};

InnerCell.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node
};

InnerCell.defaultProps = {
  className: '',
  children: null
};


const StyledInnerCell = Styled(InnerCell)`
  padding-left: 15px;
  padding-right: 15px;
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
  width: auto;

  @media (min-width: 0px) and (max-width: ${props => props.theme.mobileMaxWidth}px) {
    width: 100% !important;
    height: auto;
  }
`;

export default StyledInnerCell;
