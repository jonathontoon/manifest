import React from 'react';
import Styled from 'styled-components';
import PropTypes from 'prop-types';

import InnerCell from './InnerCell';

const HeaderCell = ({className, children}) => {
  return (
    <td className={className}>
      <InnerCell>
        {children}
      </InnerCell>
    </td>
  );
};

HeaderCell.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node
};

HeaderCell.defaultProps = {
  className: '',
  children: null
};

const StyledHeaderCell= Styled(HeaderCell)`
  width: 200px;
  flex-grow: 1;
  overflow: hidden; // Or flex might break
  background-color: transparent;
  margin-right: 2px;
  margin-bottom: 2px;
  color: ${props => props.theme.greyColor};
  font-weight: 500;
  font-size: ${props => props.theme.extraSmallFont}px;
  text-transform: uppercase;
  line-height: 30px;

  &:hover {
    cursor: pointer;
  }
`;

export default StyledHeaderCell;
