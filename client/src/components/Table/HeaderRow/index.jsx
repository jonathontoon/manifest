import React from 'react';
import Styled from 'styled-components';
import PropTypes from 'prop-types';

import Row from '../Row';

const HeaderRow = ({className, children}) => {
  return (
    <Row className={className}>
      {children}
    </Row>
  );
};

Row.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node
};

Row.defaultProps = {
  className: '',
  children: null
};

const StyledHeaderRow= Styled(HeaderRow)`
  height: 30px;
`;

export default StyledHeaderRow;
