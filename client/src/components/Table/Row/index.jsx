import React from 'react';
import Styled from 'styled-components';
import PropTypes from 'prop-types';

const Row = ({className, children}) => {
  return (
    <tr className={className}>
      {children}
    </tr>
  );
};

Row.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node
};

Row.defaultProps = {
  className: '',
  children: null
};

const StyledRow= Styled(Row)`
  width: 100%;
`;

export default StyledRow;
