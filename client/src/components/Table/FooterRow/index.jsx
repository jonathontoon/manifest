import React from 'react';
import Styled from 'styled-components';
import PropTypes from 'prop-types';

import Row from '../Row';

const FooterRow = ({className, children}) => {
  return (
    <Row className={className}>
      {children}
    </Row>
  );
};

FooterRow.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node
};

FooterRow.defaultProps = {
  className: '',
  children: null
};

const StyledFooterRow = Styled(FooterRow)`
  height: 48px;
`;

export default StyledFooterRow;
