// Based off https://github.com/mcjohnalds/react-table-drag-select/

import React, { PureComponent } from 'react';
import Styled from 'styled-components';
import PropTypes from 'prop-types';

import clone from "clone";

import HeaderRow from './HeaderRow';
import HeaderCell from './HeaderCell';
import Row from './Row';
import Cell from './Cell';

const eventToCellLocation = (e) => {
  console.log('eventToCellLocation');
  let target;
  // For touchmove and touchend events, e.target and e.touches[n].target are
  // wrong, so we have to rely on elementFromPoint(). For mouse clicks, we have
  // to use e.target.
  if (e.touches) {
    const touch = e.touches[0];
    target = document.elementFromPoint(touch.clientX, touch.clientY);
  } else {
    target = e.target.parentNode;
  }
  return {
    row: target.parentNode.rowIndex - 1,
    column: target.cellIndex
  };
}

class Table extends PureComponent {
  static propTypes = {
    className: PropTypes.string,
    columns: PropTypes.arrayOf(PropTypes.string.isRequired),
    rows: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.bool.isRequired).isRequired),
    onChange: PropTypes.func
  };

  static defaultProps = {
    className: '',
    columns: [],
    rows: [],
    onChange: () => {}
  };

  constructor(props) {
    super(props);

    this.state = {
      selectionStarted: false,
      startRow: null,
      startColumn: null,
      endRow: null,
      endColumn: null
    };

    this.renderBody = this.renderBody.bind(this);
    this.clearSelection = this.clearSelection.bind(this);
    this.handleTouchStartWindow = this.handleTouchStartWindow.bind(this);
    this.handleTouchMoveWindow = this.handleTouchMoveWindow.bind(this);
    this.handleTouchEndWindow = this.handleTouchEndWindow.bind(this);
    this.isCellBeingSelected = this.isCellBeingSelected.bind(this);
  }

  componentDidMount() {
    window.addEventListener("mousedown", this.handleTouchStartWindow);
    window.addEventListener("touchstart", this.handleTouchStartWindow);
    window.addEventListener("mousemove", this.handleTouchMoveWindow);
    window.addEventListener("touchmove", this.handleTouchMoveWindow);
    window.addEventListener("mouseup", this.handleTouchEndWindow);
    window.addEventListener("touchend", this.handleTouchEndWindow);
  };

  componentWillUnmount() {
    window.addEventListener("mousedown", this.handleTouchStartWindow);
    window.addEventListener("touchstart", this.handleTouchStartWindow);
    window.removeEventListener("mousemove", this.handleTouchMoveWindow);
    window.removeEventListener("touchmove", this.handleTouchMoveWindow);
    window.removeEventListener("mouseup", this.handleTouchEndWindow);
    window.removeEventListener("touchend", this.handleTouchEndWindow);
  };

  render() {
    const { className } = this.props;
    return (
      <table cellSpacing={0} className={className}>
        {this.renderHeader()}
        {this.renderBody()}
      </table>
    );
  }

  renderHeader() {
    const { columns } = this.props;
    return (
      <thead>
        <HeaderRow>
          {columns.map((column, j) => {
            return (
              <HeaderCell key={j}>
                {column}
              </HeaderCell>
            );
          })}
        </HeaderRow>
      </thead>
    );
  }

  renderBody() {
    const { rows } = this.props;
    const { startRow, startColumn } = this.state;
    return (
      <tbody>
        {rows && rows.map((row, i) => {
          return (
            <Row key={i}>
              {row.map((cell, j) => {
                return (
                  <Cell
                    key={j}
                    selected={rows[i][j]}
                    beingSelected={this.isCellBeingSelected(i, j)}
                    firstSelection={(startRow === i && startColumn === j)}
                  >
                    Cell
                  </Cell>
                );
              })}
            </Row>
          );
        })}
      </tbody>
    );
  }

  clearSelection() {
    this.setState({
      selectionStarted: false,
      startRow: null,
      startColumn: null,
      endRow: null,
      endColumn: null
    });
    const rows = clone(this.props.rows);
    for (let i = 0; i < rows.length; i++) {
      for (let j = 0; j < rows[i].length; j++) {
        rows[i][j] = false;
      }
    }
    this.props.onChange(rows);
  }

  handleTouchStartWindow(e) {
    const isLeftClick = e.button === 0;
    const isTouch = e.type !== "mousedown";
    if (!this.state.selectionStarted && (isLeftClick || isTouch)) {
      e.preventDefault();
      this.clearSelection();
      const { row, column } = eventToCellLocation(e);
      this.setState({
        selectionStarted: true,
        startRow: row,
        startColumn: column,
        endRow: row,
        endColumn: column
      });
    }
  }

  handleTouchMoveWindow(e) {
    if (this.state.selectionStarted) {
      const { row, column } = eventToCellLocation(e);
      this.setState({
        endRow: row,
        endColumn: column
      });
    }
  }

  handleTouchEndWindow(e) {
    const isLeftClick = e.button === 0;
    const isTouch = e.type !== "mousedown";
    if (this.state.selectionStarted && (isLeftClick || isTouch)) {
      const rows = clone(this.props.rows);
      const minRow = Math.min(this.state.startRow, this.state.endRow);
      const maxRow = Math.max(this.state.startRow, this.state.endRow);
      for (let row = minRow; row <= maxRow; row++) {
        const minColumn = Math.min(
          this.state.startColumn,
          this.state.endColumn
        );
        const maxColumn = Math.max(
          this.state.startColumn,
          this.state.endColumn
        );
        for (let column = minColumn; column <= maxColumn; column++) {
          rows[row][column] = true;
        }
      }
      this.props.onChange(rows);
      this.setState({
        selectionStarted: false,
      });
    }
  }

  isCellBeingSelected(row, column) {
    const minRow = Math.min(this.state.startRow, this.state.endRow);
    const maxRow = Math.max(this.state.startRow, this.state.endRow);
    const minColumn = Math.min(this.state.startColumn, this.state.endColumn);
    const maxColumn = Math.max(this.state.startColumn, this.state.endColumn);
    return (
      this.state.selectionStarted &&
      (row >= minRow &&
        row <= maxRow &&
        column >= minColumn &&
        column <= maxColumn)
    );
  };
}

const StyledTable = Styled(Table)`
  border-collapse: collapse;
`;

export default StyledTable;
