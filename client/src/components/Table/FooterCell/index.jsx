import React from 'react';
import Styled from 'styled-components';
import PropTypes from 'prop-types';

import InnerCell from './InnerCell';

const FooterCell = ({className, children}) => {
  return (
    <div className={className}>
      <InnerCell>
        {children}
      </InnerCell>
    </div>
  );
};

FooterCell.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node
};

FooterCell.defaultProps = {
  className: '',
  children: null
};

const StyledFooterCell= Styled(FooterCell)`
  width: 200px;
  flex-grow: 1;
  overflow: hidden; // Or flex might break
  background-color: ${props => props.theme.lightGreyColor};
  margin-right: 2px;
  margin-bottom: 2px;
  line-height: 24px;
  font-size: ${props => props.theme.regularFont}px;

  &:hover {
    background-color: ${props => props.theme.lightGreyHoverColor};
    cursor: pointer;
  }
`;

export default StyledFooterCell;
