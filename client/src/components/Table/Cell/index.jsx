import React, { Component } from 'react';
import Styled from 'styled-components';
import PropTypes from 'prop-types';

import InnerCell from './InnerCell';

class Cell extends Component {
  static propTypes = {
    className: PropTypes.string,
    children: PropTypes.node,
    beingSelected: PropTypes.bool,
    selected: PropTypes.bool,
    firstSelection: PropTypes.bool,
  };

  static defaultProps = {
    className: '',
    children: null,
    beingSelected: false,
    selected: false,
    firstSelection: false
  };

  // This optimization gave a 10% performance boost while drag-selecting cells
  shouldComponentUpdate(nextProps) {
    return this.props.beingSelected !== nextProps.beingSelected || this.props.selected !== nextProps.selected;
  }

  render() {
    const {
      className,
      children,
      firstSelection
    } = this.props;

    return (
      <td className={className}>
        <InnerCell
          showBorder={firstSelection}
        >
          {children}
        </InnerCell>
      </td>
    );
  }
}

const StyledCell = Styled(Cell)`
  width: 200px;
  background-color: ${(props) => {
    if(props.firstSelection) {
      return 'lightgreen';
    }

    if(props.selected || props.beingSelected) {
      return 'green';
    }

    return props.theme.lightGreyColor
  }};
  line-height: 48px;
  font-size: ${props => props.theme.regularFont}px;

  &:hover {
    cursor: pointer;
  }
`;

export default StyledCell;
