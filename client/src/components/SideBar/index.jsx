import React from 'react';
import Styled from 'styled-components';
import PropTypes from 'prop-types';

import AccountBar from '../AccountBar';
import OrganizationBar from '../OrganizationBar';

const SideBar = ({ className, children }) => {
  return (
    <div className={className}>
      <AccountBar />
      <OrganizationBar />
      {children}
    </div>
  );
};

SideBar.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node
};

SideBar.defaultProps = {
  className: '',
  children: null
};

const StyledSidebar = Styled(SideBar)`
  background-color: ${(props) => props.theme.primaryColor};
  position: absolute;
  top: 0px;
  left: 0px;
  width: 300px;
  height: 100%;
  overflow: hidden;

  @media (min-width: 0px) and (max-width: ${(props) => props.theme.mobileMaxWidth}px) {
    width: 100%;
    height: 58px;
  }
`;

export default StyledSidebar;
