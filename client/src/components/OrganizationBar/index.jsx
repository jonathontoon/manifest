import React from 'react';
import Styled from 'styled-components';
import PropTypes from 'prop-types';

const OrganizationBar = ({className, children}) => {
  return (
    <div className={className}>
      {children}
    </div>
  );
};

OrganizationBar.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node
};

OrganizationBar.defaultProps = {
  className: '',
  children: null
};

const StyledOrganizationBar = Styled(OrganizationBar)`
  background-color: ${props => props.theme.primaryColor};
  position: absolute;
  top: 0px;
  left: 60px;
  width: 240px;
  height: 100%;
  overflow: auto;

  @media (min-width: 0px) and (max-width: ${props => props.theme.mobileMaxWidth}px) {
    width: 100%;
    height: 58px;
    left: 0px;
  }
`;

export default StyledOrganizationBar;
