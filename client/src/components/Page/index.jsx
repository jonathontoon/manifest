import React from 'react';
import Styled from 'styled-components';
import PropTypes from 'prop-types';

import InnerPage from './InnerPage';
// import Header from './Header';

const Page = ({className, children}) => {
  return (
    <div className={className}>
      {/* <Header /> */}
      <InnerPage>
        {children}
      </InnerPage>
    </div>
  );
};

Page.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node
};

Page.defaultProps = {
  className: '',
  children: null
};

const StyledPage = Styled(Page)`
  background-color: white;
  position: absolute;
  width: 100%;
  height: 100%;
  overflow: hidden;

  @media (min-width: 0px) and (max-width: ${props => props.theme.mobileMaxWidth}px) {
    width: 100%;
    overflow-x: hidden;
    overflow-y: auto;
  }
`;

export default StyledPage;
