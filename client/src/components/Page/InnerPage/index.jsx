import React from 'react';
import Styled from 'styled-components';
import PropTypes from 'prop-types';

const InnerPage = ({className, children}) => {
  return (
    <div className={className}>
      {children}
    </div>
  );
};

InnerPage.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node
};

InnerPage.defaultProps = {
  className: '',
  children: null
};


const StyledInnerPage = Styled(InnerPage)`
  height: calc(100% - 90px);
  padding: ${props => props.theme.margin}px;
  overflow: auto;
`;

export default StyledInnerPage;
