import React from 'react';
import Styled from 'styled-components';
import PropTypes from 'prop-types';

const Nav = ({ className, children }) => {
  return (
    <div className={className}>
      {children}
    </div>
  );
};

NavItem.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node
};

NavItem.defaultProps = {
  className: '',
  children: null
};

const StyledNav = Styled(Nav)`
  height: 100%;
  line-height: 60px;

  @media (min-width: 0px) and (max-width: ${props => props.theme.mobileMaxWidth}px) {
    line-height: 54px;
    overflow: hidden;
    overflow-x: auto;
  }
`;

export default StyledNav;
