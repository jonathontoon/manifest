import React from 'react';
import Styled from 'styled-components';
import PropTypes from 'prop-types';

const NavItem = ({ className, children, selected }) => {
  return (
    <div className={className}>
      {children}
    </div>
  );
};

NavItem.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node,
  selected: PropTypes.bool
};

NavItem.defaultProps = {
  className: '',
  children: null,
  selected: false
};

const StyledNavItem = Styled(NavItem)`
  display: inline-block;
  padding-left: ${props => props.theme.margin}px;
  padding-right: ${props => props.theme.margin}px;
  height: calc(100% - 2px);
  font-weight: ${props => props.selected ? '500' : 'normal'};
  font-size: ${props => props.theme.regularFont}px;
  color: ${props => props.theme.blackColor};
  border-bottom: 2px solid ${props => props.selected ? props.theme.primaryColor : 'transparent'};

  &:last-child {
    margin-right: 0px;
  }

  &:hover {
    cursor: pointer;
    background-color: ${props => props.theme.lightGreyColor};
  }
`;

export default StyledNavItem;
