import React from 'react';
import Styled from 'styled-components';
import PropTypes from 'prop-types';

import Nav from './Nav';
import NavItem from './NavItem';

const PageHeader = ({ className, children }) => {
  return (
    <div className={className}>
      <Nav>
        <NavItem selected={true}>Boards</NavItem>
        <NavItem>Timeline</NavItem>
        <NavItem>Documents</NavItem>
        <NavItem>History</NavItem>
      </Nav>
      {children}
    </div>
  );
};

PageHeader.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node
};

PageHeader.defaultProps = {
  className: '',
  children: null
};

const StyledPageHeader = Styled(PageHeader)`
  border-bottom: 1px solid #EFEFEF;
  height: 60px;
  position: relative;
  top: 0px;
  left: 0px;
  padding-left: ${props => props.theme.margin}px;
  padding-right: ${props => props.theme.margin}px;

  @media (min-width: 0px) and (max-width: ${props => props.theme.mobileMaxWidth}px) {
    padding-left: 0px;
    padding-right: 0px;
    height: 52px;
    overflow: hidden;
  }
`;

export default StyledPageHeader;
