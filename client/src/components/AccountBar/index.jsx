import React from 'react';
import Styled from 'styled-components';
import PropTypes from 'prop-types';

const AccountBar = ({ className, children }) => {
  return (
    <div className={className}>
      {children}
    </div>
  );
};

AccountBar.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node
};

AccountBar.defaultProps = {
  className: '',
  children: null
};

const StyledAccountBar = Styled(AccountBar)`
  background-color: ${((props) => props.theme.blackColor)};
  position: absolute;
  top: 0px;
  left: 0px;
  width: 60px;
  height: 100%;
  overflow: auto;

  @media (min-width: 0px) and (max-width: ${(props) => props.theme.mobileMaxWidth}px) {
    left: -68px;
  }
`;

export default StyledAccountBar;
