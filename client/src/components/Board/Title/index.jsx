import React from 'react';
import Styled from 'styled-components';
import PropTypes from 'prop-types';

const Title = ({className, children}) => {
  return (
    <div className={className}>
      {children}
    </div>
  );
};

Board.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node
};

Board.defaultProps = {
  className: '',
  children: null
};

const StyledTitle = Styled(Title)`
  font-size: ${props => props.theme.largeFontSize}px;
  font-weight: 600;
  color: ${props => props.theme.blackColor};
  width: 50%;
  padding-top: ${props => props.theme.margin}px;
`;

export default StyledTitle;