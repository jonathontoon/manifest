import React from 'react';
import Styled from 'styled-components';
import PropTypes from 'prop-types';

import Header from './Header';
import Title from './Title';

const Board = ({ title, className, children }) => {
  return (
    <div className={className}>
      <Header>
        <Title>
          {title}
        </Title>
      </Header>
      {children}
    </div>
  );
};

Board.propTypes = {
  title: PropTypes.string,
  className: PropTypes.string,
  children: PropTypes.node
};

Board.defaultProps = {
  title: '',
  className: '',
  children: null
};

const StyledBoard = Styled(Board)`
  width: auto;
  height auto;
  padding-left: ${props => props.theme.margin * 2}px;
  padding-right: ${props => props.theme.margin * 2}px;
  float: left;
  margin-bottom: ${props => props.theme.margin * 2}px;
`;

export default StyledBoard;
