import React from 'react';
import Styled from 'styled-components';
import PropTypes from 'prop-types';

const Header = ({ className, children }) => {
  return (
    <div className={className}>
      {children}
    </div>
  );
};

Header.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node
};

Header.defaultProps = {
  className: '',
  children: null
};

const StyledHeader = Styled(Header)`
  background-color: blue;
  height: 80px;
  width: auto;
`;

export default StyledHeader;