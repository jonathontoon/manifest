export default {
  primaryColor: "#3F51B5",
  blackColor: "#0a122b",
  greyColor: "#a7adba",
  lightGreyColor: "#FAFAFA",
  darkGreySelectColor: "#F0F0F0",
  lightGreyHoverColor: "#F7F7F7",
  whiteColor: "#ffffff",

  extraLargeFont: "24",
  largeFont: "20",
  regularFont: "16",
  smallFont: "14",
  extraSmallFont: "12",
  extraExtraSmallFont: "10",
  lineHeight: "20",

  margin: "15",
  mobileMaxWidth: 1024,
  mobileMaxHeight: 1366
};