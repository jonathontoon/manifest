import React, { PureComponent } from 'react';
import Styled from 'styled-components';
import PropTypes from 'prop-types';

// import SideBar from '../../components/SideBar';
import Page from '../../components/Page';
import Table from '../../components/Table';

class Main extends PureComponent {
  static propTypes = {
    className: PropTypes.string,
    children: PropTypes.node
  };

  static defaultProps = {
    className: ''
  };

  constructor(props) {
    super(props);

    this.state = {
      columns: [],
      rows: [
        [false, false, false, false, false],
        [false, false, false, false, false],
        [false, false, false, false, false],
        [false, false, false, false, false],
        [false, false, false, false, false],
        [false, false, false, false, false],
        [false, false, false, false, false]
      ]
    };

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(rows) {
    this.setState({
      rows
    });
  }

  render() {
    const { className } = this.props;
    const { columns, rows } = this.state;
    return (
      <main className={className}>
        {/* <SideBar /> */}
        <Page>
          <Table
            columns={columns}
            rows={rows}
            onChange={this.handleChange}
          />
        </Page>
      </main>
    );
  }
}

const MainStyled = Styled(Main)`
  width: 100%;
  height: 100%;
`;

export default MainStyled;
