import pgPromise from 'pg-promise';

import config from './config';

const pgp = pgPromise({});
const database = pgp(config);

export default database;
