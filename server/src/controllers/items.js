import db from '../database';

export const getAllItems = (req, res) => {
  db.any('SELECT * FROM items ORDER BY item_id ASC').then((data) => {
    res.status(200).json({
      status: 200,
      message: 'Retrived all items',
      data,
    });
  }).catch((err) => {
    console.error(err);
    res.status(400).json({
      status: 400,
      error: ['Could not retrieve any items.'],
    });
  });
};

export const getSingleItem = (req, res) => {
  const itemId = parseInt(req.params.item_id, 10);

  db.one('SELECT * FROM items WHERE item_id = $1', itemId).then((data) => {
    res.status(200).json({
      status: 200,
      message: 'Retrieved 1 item',
      data,
    });
  }).catch((err) => {
    console.error(err);
    res.status(400).json({
      status: 400,
      error: ['No item exists with this id.'],
    });
  });
};

export const createItem = (req, res) => {
  const { title, description } = req.body;

  db.one(
    'INSERT INTO items(title, description) VALUES($1, $2) RETURNING item_id',
    [title, description],
  ).then((data) => {
    res.status(201).json({
      status: 201,
      message: 'Item was created.',
      data,
    });
  }).catch((err) => {
    console.error(err);
    res.status(400).json({
      status: 400,
      error: ['Item failed to be created.'],
    });
  });
};

export const updateItem = (req, res) => {
  const itemId = parseInt(req.params.item_id, 10);
  const { title, description, updatedAt } = req.body;

  db.none(
    'UPDATE items SET title=$1, description=$2, updated_at=$3 WHERE item_id=$4',
    [title, description, updatedAt, itemId],
  ).then(() => {
    res.status(200).json({
      status: 201,
      message: 'Updated item.',
    });
  }).catch((err) => {
    console.error(err);
    res.status(400).json({
      status: 400,
      error: ['Item failed to be updated.'],
    });
  });
};

export const deleteItem = (req, res) => {
  const itemId = parseInt(req.params.item_id, 10);

  db.result('DELETE FROM items WHERE item_id = $1', itemId).then((result) => {
    res.status(200).json({
      status: 200,
      message: `Removed ${result.rowCount} item successfully.`,
    });
  }).catch((err) => {
    console.error(err);
    res.status(400).json({
      status: 400,
      error: ['Item failed to be updated.'],
    });
  });
};
