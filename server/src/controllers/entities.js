import db from '../database';

export const getAllEntitiesForItem = (req, res) => {
  console.log(req.params);
  const itemId = parseInt(req.params.item_id, 10);

  db.any('SELECT * FROM entities WHERE item_id = $1 ORDER BY entity_id ASC', itemId).then((data) => {
    res.status(200).json({
      status: 200,
      message: 'Retrived all entities for item.',
      data,
    });
  }).catch((err) => {
    console.error(err);
    res.status(400).json({
      status: 400,
      error: ['Could not retrieve any entities for this item.'],
    });
  });
};

export const getSingleEntity = (req, res) => {
  const entityId = parseInt(req.params.entity_id, 10);

  db.one('SELECT * FROM entities WHERE entity_id = $1', entityId).then((data) => {
    res.status(200).json({
      status: 200,
      message: 'Retrieved 1 entity',
      data,
    });
  }).catch((err) => {
    console.error(err);
    res.status(400).json({
      status: 400,
      error: ['No entity exists with this id.'],
    });
  });
};

export const createEntity = (req, res) => {
  const { title, description } = req.body;

  db.one(
    'INSERT INTO entities(title, description) VALUES($1, $2) RETURNING entity_id',
    [title, description],
  ).then((data) => {
    res.status(201).json({
      status: 201,
      message: 'Entity was created.',
      data,
    });
  }).catch((err) => {
    console.error(err);
    res.status(400).json({
      status: 400,
      error: ['Entity failed to be created.'],
    });
  });
};

export const updateEntity = (req, res) => {
  const itemId = parseInt(req.params.item_id, 10);
  const { title, description, updatedAt } = req.body;

  db.none(
    'UPDATE entities SET title=$1, description=$2, updated_at=$3 WHERE entity_id=$4',
    [title, description, updatedAt, itemId],
  ).then(() => {
    res.status(200).json({
      status: 201,
      message: 'Updated entity.',
    });
  }).catch((err) => {
    console.error(err);
    res.status(400).json({
      status: 400,
      error: ['Entity failed to be updated.'],
    });
  });
};

export const deleteEntity = (req, res) => {
  const itemId = parseInt(req.params.id, 10);
  db.result('DELETE FROM entities WHERE entity_id = $1', itemId).then((result) => {
    res.status(200).json({
      status: 200,
      message: `Removed ${result.rowCount} item successfully.`,
    });
  }).catch((err) => {
    console.error(err);
    res.status(400).json({
      status: 400,
      error: ['Entity failed to be updated.'],
    });
  });
};
