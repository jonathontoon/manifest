import express from 'express';
import bodyParser from 'body-parser';
import logger from 'morgan';
import cookieParser from 'cookie-parser';

import itemsRoute from './routes/items';

import db from './database';

const app = express();

db.connect().then(() => {
  // Which port to listen on
  app.set('port', process.env.PORT || 3000);

  // Start listening for HTTP requests
  const server = app.listen(app.get('port'), () => {
    const { port } = server.address();
    console.log(`Listening on ${port}`);
  });

  app.use(logger('dev'));
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(cookieParser());

  app.all('*', (req, res, next) => {
    res.header('Content-Type', 'application/json');
    next();
  });

  app.get('/', (req, res) => {
    res.redirect('/api');
  });

  app.get('/api', (req, res) => {
    res.json({ message: 'Let\'s Manifest' });
  });

  app.use('/api/items', itemsRoute);
}).catch((error) => {
  console.log('ERROR:', error);
});
