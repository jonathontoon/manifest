import express from 'express';

import {
  getAllEntitiesForItem,
  getSingleEntity,
  createEntity,
  updateEntity,
  deleteEntity,
} from '../controllers/entities';

const entityRouter = express.Router({
  mergeParams: true,
});

entityRouter.get('/', getAllEntitiesForItem);
entityRouter.get('/:entity_id', getSingleEntity);
entityRouter.post('/', createEntity);
entityRouter.put('/:entity_id', updateEntity);
entityRouter.delete('/:entity_id', deleteEntity);

export default entityRouter;
