import express from 'express';

import { getAllItems, getSingleItem, createItem, updateItem, deleteItem } from '../controllers/items';
import entitiesRouter from './entities';

const itemRouter = express.Router();

itemRouter.get('/', getAllItems);
itemRouter.get('/:item_id', getSingleItem);
itemRouter.post('/', createItem);
itemRouter.put('/:item_id', updateItem);
itemRouter.delete('/:item_id', deleteItem);

itemRouter.use('/:item_id/entities', entitiesRouter);

export default itemRouter;
