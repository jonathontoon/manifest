DROP DATABASE IF EXISTS manifest;
CREATE DATABASE manifest;

\c manifest;

CREATE TABLE items (
  item_id SERIAL PRIMARY KEY,
  title VARCHAR,
  description VARCHAR,
  created_at TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE entities (
  entity_id SERIAL PRIMARY KEY,
  text VARCHAR,
  created_at TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
  item_id INT references items (item_id)
);

INSERT INTO items (title, description)
VALUES (
  'Untitled 1',
  'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras volutpat accumsan lectus vitae pulvinar.'
), (
  'Untitled 2',
  'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras volutpat accumsan lectus vitae pulvinar.'
);

INSERT INTO entities (text, item_id)
VALUES (
  'Test Text 1',
  (SELECT item_id from items WHERE title='Untitled 1')
), (
  'Test Text 2',
  (SELECT item_id from items WHERE title='Untitled 1')
), (
  'Test Text 3',
  (SELECT item_id from items WHERE title='Untitled 1')
), (
  'Test Text 4',
  (SELECT item_id from items WHERE title='Untitled 1')
), (
  'Test Text 5',
  (SELECT item_id from items WHERE title='Untitled 1')
), (
  'Test Text 1',
  (SELECT item_id from items WHERE title='Untitled 2')
), (
  'Test Text 2',
  (SELECT item_id from items WHERE title='Untitled 2')
), (
  'Test Text 3',
  (SELECT item_id from items WHERE title='Untitled 2')
), (
  'Test Text 4',
  (SELECT item_id from items WHERE title='Untitled 2')
), (
  'Test Text 5',
  (SELECT item_id from items WHERE title='Untitled 2')
);